// set the timer for slideshow in seconds



ciHud.urlList = [
	{ url: "http://www.steyr-werke.at", duration: 60 },
	{ url: "http://intern.steyr-werke.at", duration: 12 },
	{ url: "http://intern.steyr-werke.at/index.php?r=directory%2Fdirectory%2Fspaces", duration: 60 },
	{ url: "http://tetrapix.steyr-werke.at", duration: 30 },
	{ url: "http://tonnturbo.at", duration: 30 },
	{ url: "http://octopi.local/#control", duration: 30 },
	{ url: "http://192.168.1.192:8123/lovelace/default_view", durattion: 30 },
	{ url: "http://192.168.1.48/admin/", durattion: 30 },
	{ url: "https://opensensemap.org/explore/5e17ab85abd537001a757948", duration: 30 },
	{ url: "https://opensensemap.org/explore/5e1653b57e9052001a52465f", duration: 30 },
	{ url: "https://maps.sensor.community/#11/48.0837/14.3966", duration: 30 },
	{ url: "MEDIA/Video_2017/Maker_Faire_Public_Display_v02_1080p_base_cbr_3mbps.mp4", duration: 35 },
	{ url: "MEDIA/Video_2017/Maker_Faire_Public_Display_v02_1080p_main_vbr_3to5mbps.mp4", duration: 156 },
	{ url: "MEDIA/Video_2017/Maker_Faire_Public_display_v02.mp4", duration: 30 },
	{ url: "MEDIA/Video_2017/MMF_2017_Nachbericht.mp4", duration: 174 },
	{ url: "MEDIA/Video_2017/Steyrwerke_Flugaufnahmen_V02.mp4", duration: 22 }
];

ciHud.urlList2 = [
	{ url: "http://www.steyr-werke.at", duration: 60 },
	{ url: "http://intern.steyr-werke.at", duration: 12 },
	{ url: "http://intern.steyr-werke.at/index.php?r=directory%2Fdirectory%2Fspaces", duration: 60 },
	{ url: "http://nossl.hofbauer.xyz/ChIoT/", duration: 20 },
	{ url: "https://opensensemap.org/explore/5e17ab85abd537001a757948", duration: 20 },
	{ url: "https://maps.sensor.community/#11/48.0837/14.3966", duration: 20 },
	{ url: "http://usb3000.steyr-werke.at:8123/lovelace", duration: 25 },
	{ url: "MEDIA/raspberrypi_large.jpg", duration: 10 },
	{ url: "MEDIA/IMG_20170507_201653.jpg", duration: 10 },
	{ url: "MEDIA/MAW_280417_cz20357.jpg", duration: 10 },
	{ url: "MEDIA/MAW_280417_cz20676.jpg", duration: 10 },
	{ url: "MEDIA/MAW_280417_cz20718.jpg", duration: 10 },
	{ url: "https://www.instagram.com/steyr_werke", duration: 45 },
	{ url: "https://www.twitter.com/steyrwerke", duration: 45 },
	{ url: "https://www.facebook.com/steyrwerke", duration: 45 },
	{ url: "https://www.steyr-werke.at/aktuell/", duration: 60 },
	{ url: "https://www.steyr-werke.at/ausstattung/", duration: 60 },
	{ url: "https://www.steyr-werke.at/aussteller", duration: 60 },
	{ url: "https://www.steyr-werke.at/termine", duration: 60 },
	{ url: "https://www.steyr-werke.at/profactor-openlab/", duration: 45 },
	{ url: "https://www.steyr-werke.at/repair-cafe/", duration: 45 },
	{ url: "MEDIA/Video_2017/Maker_Faire_Public_Display_v02_1080p_base_cbr_3mbps.mp4", duration: 35 },
	{ url: "MEDIA/Video_2017/Maker_Faire_Public_Display_v02_1080p_main_vbr_3to5mbps.mp4", duration: 156 },
	{ url: "MEDIA/Video_2017/Maker_Faire_Public_display_v02.mp4", duration: 30 },
	{ url: "MEDIA/Video_2017/MMF_2017_Nachbericht.mp4", duration: 174 },
	{ url: "MEDIA/Video_2017/Steyrwerke_Flugaufnahmen_V02.mp4", duration: 22 }
];

old = [
	{ url: "dummy/page1.html", duration: 1 },
	{ url: "dummy/page2.html", duration: 2 },
	{ url: "dummy/page3.html", duration: 3 },
	{ url: "dummy/page4.html", duration: 4 },
	{ url: "dummy/page5.html", duration: 5 },


];
