// set the timer for slideshow in milliseconds


ciHud.urlList = [
	{ url: "http://steyr-werke.at", duration: 45 },
	{ url: "https://www.steyr-werke.at/aussteller/", duration: 45 }, 
	{ url: "https://www.steyr-werke.at/workshops/", duration: 45 }, 
	{ url: "dummy/IMG_20170507_201653.jpg", duration: 5 },
	{ url: "https://www.steyr-werke.at/ausstattung/", duration: 45 }, 
	{ url: "https://www.steyr-werke.at/profactor-openlab/", duration: 45 }, 
	{ url: "https://www.steyr-werke.at/repair-cafe/", duration: 45 },
	{ url: "https://www.steyr-werke.at/termine/", duration: 22 },
	{ url: "http://intern.steyr-werke.at", duration: 12 },
	{ url: "img/MF_Public_Display_2017/Maker_Faire_Public_Display_v02_1080p_main_vbr_3to5mbps.mp4", duration: 160 }
];
