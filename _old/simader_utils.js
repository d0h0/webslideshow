

dh.init = {};

/*
 * Init Waypoints fade in effect
 */
dh.init.Waypoints = function() {
    //Animate on scroll: menu contents
    $("#philosophieContent").waypoint(function() {
        $('#philosophieContent').addClass('animated fadeIn');
    }, { offset: '100%'});
    $("#serviceContent").waypoint(function() {
        $('#serviceContent').addClass('animated fadeIn');
    }, { offset: '100%'});
    $("#impressumContent").waypoint(function() {
        $('#impressumContent').addClass('animated fadeIn');
    }, { offset: '100%'});

    // service list bullets
    $("#serviceContent").waypoint(function() {
        $('.icon').addClass('animated flip');
    }, { offset: '100%'});

    // service list icon hover effect
    var serviceItem = $('.serviceItem');
    var bulletPoints = $('.serviceItem .icon')
    var pulse = 'animated swing';
    serviceItem.hover(function() {
        //$(this).addClass(pulse);
        $(this).find('.icon').addClass(pulse);
        //serviceItem.css('background', 'red');
    },function() {
        bulletPoints.removeClass(pulse);
    });
//        bulletPoints.waypoint(function() {
//            bulletPoints.addClass(animationClass);
//            console.log("added");
//        }, { offset: '100%'});
//        $('#impressumContent').waypoint(function() {
//            bulletPoints.removeClass(animationClass);
//            console.log("rm");
//        }, { offset: '100%'});
};

/*
 * Init Scroll down to navigation bar
 */
dh.init.ScrollToNavBar = function() {
    scrollToId("#atelierLink", "slow");
};

/*
 * Load Scroll down helper function
 */
function scrollToId(id, scrollSpeed) { //this function is not visible for the onClick function
    var divLoc = jQuery(id).offset();
    console.log(divLoc);
    jQuery('html,body').animate({ scrollTop: divLoc.top }, scrollSpeed);
};

/*
 * Register Click Events for TOP buttons
 */
dh.init.ScrollTopClickEvents = function() {
    jQuery('.top').click(function() {
        jQuery('html,body').animate({ scrollTop: 0 }, 'fast');
        return false;
    });
};

/*
 * Init atelier navigation
 */
dh.init.atelierNavigation = function() {
    jQuery('#slider1Link').click(function() { loadSliderImages(1); });
    jQuery('#slider2Link').click(function() { loadSliderImages(2); });
    jQuery('#slider3Link').click(function() { loadSliderImages(3); });
}

/**
 * Load atelier slider helper function
 */
var loadSliderImages = function(listId) {
    //loadsliderImages
    switch(listId) {
        case 1: images = dh.images1; break;
        case 2: images = dh.images2; break;
        case 3: images = dh.images3; break;
    }

    for(var i = 1; i<images.length+1; i++) {
        var j = i-1; //i=.item-elements, j=array-elements
        var img = jQuery('.carousel-inner .item:nth-child('+i+') img')
        img.attr('src', images[j].src);
        var desc = jQuery('.carousel-inner .item:nth-child('+i+') .slideText')
        desc.html(images[j].desc + "<br>");
    }
};

/*
 * Init navigation for impressum fade in
 */
dh.init.ImpressumNavigation = function() {
    jQuery('.meLink').click(function() {
        jQuery('.impressum').hide();
        jQuery('.me').fadeIn();
        return false;
    });
    jQuery('.pressLink').click(function() {
        jQuery('.impressum').hide();
        jQuery('.press').fadeIn();
        return false;
    });
    jQuery('.moreLink').click(function() {
        jQuery('.impressum').hide();
        jQuery('.more').show();
        return false;
    });

    //add waypoint to slide in
    jQuery('#impressum').waypoint(function() {
        jQuery('.impressum.more').addClass('animated rotateInDownRight');
        jQuery('.impressum.more').show();

        console.log("jo");
    }, { offset: '100%'});
};





/**
* DEBUG FUNCTIONS FOR CHANGING BACKGROUND etc...
*/
dh.dbg = {};

dh.dbg = function() {
    dh.dbg.initChangeBg(); //load list of available backgrounds and show list.
    dh.dbg.clickChangeBg(); //init click on item to change background

    dh.dbg.initDhControlPanel(); //init dh debug control panel
}

//init dh debug control panel
dh.dbg.initDhControlPanel = function() {
    jQuery('.dhControlPanel').hover(function() {
        jQuery(this).find('.dhControlPanelIcon').animate({opacity: 0});
        jQuery(this).animate({left: '+=100'}, 200, function() {  });
        jQuery(this).animate({height: '+=400'}, 800, function() {  });
    }, function() {
        jQuery(this).find('.dhControlPanelIcon').animate({opacity: 1});
        jQuery(this).animate({height: '-=400'}, 800, function() {  });
        jQuery(this).animate({left: '-=100'}, 200, function() {  });
    });
};

//load click function for bg debug box
dh.dbg.clickChangeBg = function(fileSrc) {
    jQuery('.bgBtn img').click(function(event) {
        var eventId = event.target.id; //get ID of clicked image for getting bg img src
        var listId = eventId.substring(eventId.indexOf('_')+3, eventId.length);
        var imgId = eventId.substring(eventId.indexOf('_')+1, eventId.indexOf('_')+2);
        var cssBgValue= 'url("'
             + event.target.src
             + '")';

        jQuery('body').css('background-image', cssBgValue);
        jQuery('.dBtnMsg').html('<div>'+ event.target.title +' ' + event.target.src + ' </div>');
        jQuery('#'+event.target.id).css('box-shadow','0 0 3px 1px red');
    });
}

//load list of available backgrounds and show list
dh.dbg.initChangeBg = function() {
    //set possible backgrounds
    var addButtonToSpan = function(listId, imgList) {
        for(var i=0; i<imgList.length; i++) {
            jQuery('.bgBtnBox').append(
                '<span class="bgBtn"'
                + '>'
                + '<img src="' + imgList[i].src + '"'
                + '     id="bgBtn_' + listId + '-' + i + '"'
                + '     title="' + listId + '-' + i + '"'
                + '/>'
                );
        }
    }

    //usage: addbuttontospan(counter, list)
    addButtonToSpan(0, dh.bgImages);
    jQuery('.bgBtnBox').append('<br>\n');

    addButtonToSpan(1, dh.bgImagesDefault);
    jQuery('.bgBtnBox').append('<br>\n');

    addButtonToSpan(1, dh.images1);
    addButtonToSpan(2, dh.images2);
    addButtonToSpan(3, dh.images3);
    jQuery('.bgBtnBox').append('<br>\n');
};


/*
 * Testing...
 */
console.log("start");
var myF1 = function() { console.log("start f1: var myF1"); }
myF2 = function() { console.log("start f2: something = function()"); }
myF1();
myF2();
myF3();
function myF3() { console.log("start function"); } //this would be also visible for the onclick function in dom

// Resumee: bester Weg ist ein closure für den Namespace und ein var für funktionen für "lazy function loading"
//  - Closure: natürlich mit doppelter Klammerung, um sicher einen eigenen Namespace zu bekommen,
//             bei dem der parent nicht mit im kontext ist.
//  - var functions: myClickF1 ist der beste weg weil die funktion nicht gleich ausgeführt wird (TODO nachlesen)





