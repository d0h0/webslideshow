var dh = {}; //base object for data

dh.desc = "Simader restaurierung website";

dh.images1 = [
  {
    "name": "1",
    "src": "img2/atelier/slider1-8-sitzgruppe.jpg",
    "desc": "<h4>Biedermeier-<br>garnitur, <br> Kirschbaum,<br>um 1840 </h4><br><br> Foto nach der Restaurierung"
  },
  {
    "name": "2",
    "src": "img2/atelier/slider1-9-biedermeierstuhl_nuss.jpg",
    "desc": "<h4>Biedermeierstuhl <br>(Fächerstuhl), <br> Nussbaum<br>um 1820/1830 </h4><br><br> Foto nach der Restaurierung"
  },
  {
    "name": "3",
    "src": "img2/atelier/slider1-12-schreibkomode_offen.jpg",
    "desc": "<h4>Biedermeier-<br>schreibkommode, <br> Nussbaum<br>um 1830 </h4><br><br> Foto <br>nach der Restaurierung"
  }
];

dh.images2 = [
  {
    "name": "1",
    "src": "img2/atelier/slider2-1-biedermeier_vitrine.jpg",
    "desc": "<h4>Biedermeiervitrine <br> Mahagoni<br>um 1820/1830 </h4><br><br> Foto nach der Restaurierung"
  },
  {
    "name": "2",
    "src": "img2/atelier/slider2-2-biedermeier_schrank_kirsche.jpg",
    "desc": "<h4>Biedermeierschrank <br>Kirschbaum<br>um 1830 </h4><br><br> Foto nach der Restaurierung"
  },
  {
    "name": "3",
    "src": "img2/atelier/slider2-3-biedermeier_schrank_nuss.jpg",
    "desc": "<h4>Biedermeierschrank, <br>Nussbaum<br>um 1820 </h4><br><br> Foto nach der Restaurierung"
  }
];

dh.images3 = [
  {
    "name": "1",
    "src": "img2/atelier/slider3-13-mohr_als_gueridon.jpg",
    "desc": "<h4>Mohr als Gueridon <br><small>19. Jahrhundert</small></h4> <br> polychrom gefasst<br> mit Blattmetall belegt<br> Foto nach der Restaurierung"
  },
  {
    "name": "2",
    "src": "img2/atelier/slider3-14-spiegelrahmen_vor_restaurierung.jpg",
    "desc": "<h4>Spiegelrahmen <br><small>um 1830 </small></h4><br> vergoldet <br> Foto vor der Restaurierung"
  },
  {
    "name": "3",
    "src": "img2/atelier/slider3-15-spiegelrahmen_nach_restaurierung.jpg",
    "desc": "<h4>Spiegelrahmen <br><small>um 1830 </small></h4><br> vergoldet <br> Foto nach der Restaurierung"
  }
];


dh.bgImages = [
  {
    "name": "1",
    "src": "img2/bg/books-1655783_960_720.jpg",
    "desc": "text"
  },
  {
    "name": "2",
    "src": "img2/bg/floor-1256804_960_720.jpg",
    "desc": "text"
  },
  {
    "name": "3",
    "src": "img2/bg/sky-1813050_960_720.png",
    "desc": "text"
  },
  {
    "name": "4",
    "src": "img2/bg/abstract-1846967_960_720.jpg",
    "desc": "text"
  }
];

dh.bgImagesDefault = [
  {
    "name": "1",
    "src": "img/bg.jpg",
    "desc": "text"
  },
  {
    "name": "2",
    "src": "img/bg_original.jpg",
    "desc": "text"
  },
  {
    "name": "3",
    "src": "img/slide-1.jpg",
    "desc": "text"
  },
  {
    "name": "4",
    "src": "img/slide-2.jpg",
    "desc": "text"
  },
  {
    "name": "5",
    "src": "img/slide-3.jpg",
    "desc": "text"
  },
  {
    "name": "6",
    "src": "img2/bg/nuss-bg-struktur-hell.jpg",
    "desc": "text"
  },
  {
    "name": "7",
    "src": "img2/bg/nuss-bg-struktur.jpg",
    "desc": "text"
  }
];


