(function() {


    $(document).ready(function(){
        dh.init.ScrollToNavBar(); //scrolle nach unten

        dh.init.Waypoints(); // init functions

        dh.init.ScrollTopClickEvents();

        dh.init.atelierNavigation();

        dh.init.ImpressumNavigation();

        dh.dbg(); // init debug mode for bg-switch, layout-switch, font-switch...

    });

})()