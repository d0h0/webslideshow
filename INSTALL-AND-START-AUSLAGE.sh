#!/bin/bash
#
# This script is started with each system start
#

#generate ssh keys
#upload ssh keys to gitlab.com

INSTALL_DIR="/home/stw/srv/webslideshow"
cd $INSTALL_DIR

#echo "##### Install apt packages for webslideshow"
#sudo apt install -qq byobu openssh-server
#sudo apt install -qq git vim
#sudo apt install -qq chromium-browser

echo "##### Copy this autostart"
#cp $INSTALL_DIR/conf/autostart-openbox.conf ~/.config/openbox/autostart
cp $INSTALL_DIR/conf/autostart-openbox.conf ~/.config/lxsession/Lubuntu/autostart
#chmod a+x ~/.config/openbox/autostart


echo "##### git pull latest changes for WEBSLIDESHOW"
git pull

echo "##### Starting chromium"
chromium-browser --app=file://$INSTALL_DIR/index.html --start-fullscreen &
pid=$!
logger "Webslideshow started with pid $pid."

echo "Webslideshow started with pid $pid."

