#!/bin/bash

# Set ciHud status to "running"
mosquitto_pub -h localhost -p 1883 -t topic -m '{
        "id": 0, 
        "status": "running"
    }'

# Do something successful
ping -c 5 localhost;

retval=$?

if [ $retval -eq 0 ]; then
    status='success';
else
    status='failure';
fi

# Set ciHud status to "success"
mosquitto_pub -h localhost -p 1883 -t topic -m '{
        "id": 0, 
        "name": "UI FRontend",
        "message": "UPDATE FROM JENKINS", 
        "status": "'$status'",
        "css": { "widthClass": "col-md-12" }
    }'









