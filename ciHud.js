/*
 * ciHud
 */

listIndex = 0;

var incrementIndex = function() {
	if (listIndex >= ciHud.urlList.length-1) {
		listIndex = 0;
	} else {
		listIndex++;
	}
}

var decrementIndex = function() {
	if (listIndex <= 0) {
		listIndex = ciHud.urlList.length-1;
	} else {
		listIndex--;
	}
}

var setIframeSrc = function() {
	var url = ciHud.urlList[listIndex].url;

	jQuery('#content').hide();

	jQuery('#content').attr('src', url);

	jQuery('#content').ready(function() { //show things when theyre loaded
		jQuery('#content').fadeIn();
	});
}

ciHud.loadNextSlide = function() {
	setIframeSrc();
	incrementIndex();
}
ciHud.loadPrevSlide = function() {
	setIframeSrc();
	decrementIndex();
}

ciHud.initiateSlideshow = function() {
	var duration = ciHud.urlList[listIndex].duration*1000;
	console.log(listIndex, ciHud.urlList[listIndex]);
	if (ciHud.ctrl.play) { //default state: play is true
		ciHud.loadNextSlide();
	}
	setTimeout(function() {
		ciHud.initiateSlideshow();
		}, duration
	);
}

/*
 * User Interactions
 */
ciHud.togglePlay = function() {
	console.log(ciHud.ctrl.play);
	if (ciHud.ctrl.play) {
		ciHud.ctrl.play = false;
		jQuery('.playBtn .playIcon').show();
		jQuery('.playBtn .stopIcon').hide();
	} else {
		ciHud.ctrl.play = true;
		jQuery('.playBtn .playIcon').hide();
		jQuery('.playBtn .stopIcon').show();
	}
}
