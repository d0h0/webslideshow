/*
 * CiHUD - initialize objects and defaults
 */

var ciHud = {};

ciHud.mqtt = {};
ciHud.mock = {};
ciHud.util = {};

ciHud.enums = {};

ciHud.data = {};

ciHud.ctrl = {};
ciHud.ctrl.play = true;
